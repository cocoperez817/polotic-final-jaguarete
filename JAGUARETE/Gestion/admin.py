from django.contrib import admin
from Gestion.models import Categoria, Productos, Carrito

class CategoriaAdmin(admin.ModelAdmin):
    list_display=("descripcion",)
    search_fields=("descripcion",)
    
class ProductosAdmin(admin.ModelAdmin):
    list_display=("titulo","precio",)
    list_filter=("titulo","precio",)  #la coma va porque es una tupla
    
class CarritoAdmin(admin.ModelAdmin):   
    list_display=("usuario","Lista","total","entregado",)
    list_filter=("usuario","Lista","total","entregado",)     #la coma va porque es una tupla
  #  date_hierarchy=("fecha")
 
admin.site.register (Categoria,CategoriaAdmin)
admin.site.register (Productos, ProductosAdmin)
admin.site.register (Carrito, CarritoAdmin)