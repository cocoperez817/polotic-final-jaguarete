# Generated by Django 3.2.4 on 2021-07-06 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Gestion', '0006_alter_productos_imagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='productos',
            name='categorias',
            field=models.ManyToManyField(to='Gestion.Categoria'),
        ),
    ]
