# Generated by Django 3.2.4 on 2021-07-02 14:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Gestion', '0003_alter_productos_descripcion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productos',
            name='titulo',
            field=models.CharField(max_length=30, verbose_name='El Titulo'),
        ),
    ]
