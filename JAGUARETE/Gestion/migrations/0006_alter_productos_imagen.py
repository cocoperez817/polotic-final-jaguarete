# Generated by Django 3.2.4 on 2021-07-06 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Gestion', '0005_carrito_entregado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productos',
            name='imagen',
            field=models.ImageField(upload_to='Gestion/fotos'),
        ),
    ]
