
from django.db import models

# Create your models here.
class Categoria(models.Model):
    descripcion=models.CharField(max_length=100)
    
    def __str__(self):
       return self.descripcion

class Productos(models.Model):  
    titulo=models.CharField(max_length=30, verbose_name="El Titulo")      
    imagen=models.ImageField(upload_to ='Gestion/fotos')    
    descripcion=models.CharField(blank=True, null= True, max_length=100)
    precio=models.IntegerField()
    categoria=models.ManyToManyField(Categoria)    
    #categoria=models.CharField(max_length=30)
    
    def __str__(self):
        return 'El nombre del Producto es %s de la categoria: %s con precio:$%s' % (self.titulo, self.categoria, self.precio)
    
class Carrito(models.Model):
    usuario=models.CharField(max_length=30)
    Lista=models.CharField(max_length=30)
    total=models.IntegerField()
    entregado=models.BooleanField(default=False)
    
    def __str__(self):
        return 'Para %s su lista de entrega es %s y el total es %s' % (self.usuario, self.Lista, self.total)
    
