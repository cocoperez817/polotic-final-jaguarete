from django.urls import path
from Gestion import views
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    
    path('', views.index, name="index"),
    path('categoria/', views.categoria, name="categoria"),
    path('acercaDe/', views.acercaDe, name="acercaDe"),
    path('contacto/', views.contacto, name="contacto"),
    path('nuevoProducto/', views.nuevoProducto, name="nuevoProducto"),
    path('login/', views.login, name="login"),
    path('registro/', views.registro, name="registro"),
    path('carrito/', views.carrito, name="carrito"),
    path('search/', views.search, name="search"),
    path('resultados_busqueda/', views.resultados_busqueda, name="resultados_busqueda"),

    
]
#urlpatterns=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)   

#Add Django site authentication urls (for login, logout, password management)
